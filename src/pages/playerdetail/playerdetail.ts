import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalService } from './../../app/global-service'

@Component({
  selector: 'page-playerdetail',
  templateUrl: 'playerdetail.html',
})
export class PlayerdetailPage {
  player_profile: any;
  player_info: any;
  cover_pic: any;
  logo: any;
  followers: any;
  following: any;
  name: any;
  bio: any;
  city: any;
  country: any;
  gender: any;
  leg: any;
  tour: any;
  match: any;
  age: any;
  member_since: any;
  goal:any

  constructor(private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
    this.getPlayersDetail(this.navParams.get('id'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayerdetailPage');
  }
  
  getPlayersDetail(id) {
    let url = this.global.basepath + "/user/player/?form_type=profile&player=" + id + "&format=json";
    this.global.getRequest(url)
      .subscribe(res => {
        this.player_profile = res.json()
        console.log("profileeeeeeeeeeeeee", this.player_profile)
        this.cover_pic = this.player_profile.cover_pic ? this.global.imgpath + this.player_profile.cover_pic : 'assets/images/defaultplayercover.jpg';
        this.logo = this.player_profile.profile_pic ? this.global.imgpath + this.player_profile.profile_pic : 'assets/images/defaultplayer.jpg';
        this.followers = this.player_profile.follower;
        this.following = this.player_profile.following;
        this.name = this.player_profile.name;
        let url1 = this.global.basepath + "/user/player/?form_type=information&player=" + id + "&format=json";
        this.global.getRequest(url1)
          .subscribe(res => {
            this.player_info = res.json()
            console.log("infooooooooooooooo", this.player_info)
            this.bio = this.player_info.basic_detail.bio;
            this.city = this.player_info.basic_detail.city;
            this.country = this.player_info.basic_detail.country;
            this.gender = this.player_info.basic_detail.gender;
            this.leg = this.player_info.basic_detail.leg;
            this.tour = this.player_info.tournament_played;
            this.match = this.player_info.match_played;
            this.age = this.player_info.basic_detail.dob;
            this.goal = this.player_info.total_goal
          })
      })
  }

  back(){
    this.navCtrl.pop();
  }

}



