import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalService } from './../../app/global-service'
import { Http, Headers } from '@angular/http';
import { PlayerdetailPage } from '../playerdetail/playerdetail';

/**
 * Generated class for the PlayersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-players',
  templateUrl: 'players.html',
})
export class PlayersPage {
  result: any[] = [];
  header: Headers;
  total_page: number;
  cover_pic: any;
  page: number = 1;

  constructor(private http: Http, private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.getPlayersList(this.page);
  }

  getPlayersList(page, $event?) {
    let url = this.global.basepath + "user/player/?format=json"
    let data = {
      player_name: "",
      form_type: "filter",
      city: "",
      gender: [],
      position: [],
      attitude: [],
      technique: [],
      is_pro: false,
      referee: false,
      page: page,
    }
    this.global.postRequest(url, data)
      .subscribe(res => {
        this.total_page = res.json().total_pages;
        let arr = res.json().data
        Array.prototype.push.apply(this.result, arr);
        this.cover_pic = this.global.imgpath;
        if ($event) {
          $event.complete();
        }
      })
  }


  doInfinite($event) {
    this.page++;
    if (this.total_page >= this.page) {
      this.getPlayersList(this.page, $event)
    }
  }

  playerdetail(id) {
    this.navCtrl.push(PlayerdetailPage,{
      id:id
    })
  }
}