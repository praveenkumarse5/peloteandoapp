import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroundsPage } from './grounds';

@NgModule({
  declarations: [
    GroundsPage,
  ],
  imports: [
    IonicPageModule.forChild(GroundsPage),
  ],
  exports: [
    GroundsPage
  ]
})
export class GroundsPageModule {}
