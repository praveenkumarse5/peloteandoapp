import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalService } from './../../app/global-service'

/**
 * Generated class for the TeamdetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-teamdetail',
  templateUrl: 'teamdetail.html',
})
export class TeamdetailPage {
  result: any;
  result1: any;
  cover_pic: any;

  constructor(private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
    this.getTeamsDetail(this.navParams.get('id'))
  }

  getTeamsDetail(id) {
    let url = this.global.basepath + "team/profile/" + id + "/?form_type=information&format=json";
    this.global.getRequest(url)
      .subscribe(res => {
        this.result = res.json()
        let url1 = this.global.basepath + "team/profile/" + id + "/?form_type=basic&format=json";
        this.global.getRequest(url1)
          .subscribe(res => {
            this.result1 = res.json();
            this.cover_pic = this.global.imgpath;
          })
      })
  }

  back() {
    this.navCtrl.pop();
  }

}
