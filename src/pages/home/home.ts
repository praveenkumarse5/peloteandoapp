import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private loadingCtrl: LoadingController, public navCtrl: NavController) { 
    this.presentLoading();
  }

  pushPage(page) {
    this.presentLoading();
    setTimeout(res => {
      this.navCtrl.push(page)
    }, 2000)

  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }

}
