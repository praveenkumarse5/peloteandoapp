import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { GlobalService } from './../../app/global-service'
import { Http, Headers } from '@angular/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook'
//  declare const FB
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form: FormGroup;
  loginVAlid: boolean = false;

  constructor(private FB: Facebook, private http: Http, public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder, private global: GlobalService) {
    this.form = this.fb.group({
      "user": "",
      "pass": ""
    })
    if (localStorage.getItem('user') != null) {
      let current_time = new Date().getTime() / 1000;
      let expire_time = localStorage.getItem("expire")
      if (parseInt(expire_time) > current_time) {
        this.navCtrl.setRoot(HomePage)
      }
    }
  }


  login() {
    let url = this.global.basepath + "api/peloteando/login";
    this.global.loginValid(url, this.form.value)
      .subscribe(res => {
        let data = res.json()[0]
        let userInfo = {
          "token": data.token.access_token,
          "expire_date": data.token.expires_in
        }
        let expire = (new Date().getTime() / 1000) + data.token.expires_in;
        localStorage.setItem("user", JSON.stringify(userInfo))
        localStorage.setItem("expire", JSON.stringify(expire))
        this.navCtrl.setRoot(HomePage)
      });
  }

  signup() {
    this.navCtrl.setRoot(SignupPage)
  }

  fbLogin() {
    let hn = this.FB.getLoginStatus()
      .then(res => {
        let status = res.status
        if (status === "connected") {
          this.FB.api("/me?fields=name,id,birthday,picture,email", ['public_profile', 'user_friends', 'email', 'user_birthday'])
            .then(res => {
              let userInfo = {
                "name": res.name,
                "birthday": res.birthday,
                "id": res.id,
                "imageurl": res.picture.data.url
              }
              this.navCtrl.setRoot(HomePage)
            })
        }
        else {
          this.FB.login(['public_profile', 'user_friends', 'email', 'user_birthday'])
            .then(afterlogin => {
              this.FB.api("/me?fields=name,id,birthday,picture,email", ['public_profile', 'user_friends', 'email', 'user_birthday'])
                .then(res => {
                  let userInfo = {
                    "name": res.name,
                    "birthday": res.birthday,
                    "id": res.id,
                    "imageurl": res.picture.data.url
                  }
                  this.navCtrl.setRoot(HomePage)
                })
            })
        }
      })
  }
}
        //     FB.getLoginStatus(res => {
        //       if (res.status!=="connected"){
        //         FB.login(res=>{
        //           if(res.status==="connected"){
        //            let url = "https://graph.facebook.com/me?access_token="+res.authResponse.accessToken+"&fields=id,about,email,birthday,name,picture"
        //             this.http.get(url)
        //             .subscribe(res=>{
        //               console.log("responseface book ",res.json())
        //             })
        //             this.navCtrl.setRoot(HomePage);
        //           }
        //         },{scope: 'email,user_about_me,user_birthday', return_scopes: true})
        //       }
        //        else  if(res.status==="connected"){
        //            let url = "https://graph.facebook.com/me?access_token="+res.authResponse.accessToken+"&fields=birthday,about,email,first_name,last_name,gender"
        //             this.http.get(url)
        //             .subscribe(res=>{
        //             })
        //             this.navCtrl.setRoot(HomePage);
        //           }

