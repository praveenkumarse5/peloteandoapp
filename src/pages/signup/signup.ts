import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
form:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private fb: FormBuilder) {
    this.form = this.fb.group({
      "name": "",
      "pass": "",
      "mobile":"",
      "email":"",
      "lname":""
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

   
  login() {
    this.navCtrl.setRoot(LoginPage);
  }
}
