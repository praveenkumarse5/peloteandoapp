import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalService } from './../../app/global-service'
import { Http, Headers } from '@angular/http';
import { TeamdetailPage } from '../teamdetail/teamdetail';

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage {
  result: any[] = [];
  header: Headers;
  total_page: number;
  cover_pic: any;
  page: number = 1;

  constructor(private http: Http, private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.getTeamsList(this.page);
  }

  getTeamsList(page, $event?) {
    let url = this.global.basepath + "team/profile/?format=json"
    let data = {
      team_name: "",
      form_type: "filter",
      city: "",
      gender: [],
      attitude: [],
      technique: [],
      page: page,
    }
    this.global.postRequest(url, data)
      .subscribe(res => {
        this.total_page = res.json().total_pages;
        let arr = res.json().teams
        Array.prototype.push.apply(this.result, arr);
        this.cover_pic = this.global.imgpath;
        console.log("resultteam=>>>>>>>>>>>>>>>>",this.result)
        if ($event) {
          $event.complete();
        }
      })
  }

  doInfinite($event) {
    this.page++;
    if (this.total_page >= this.page) {
      this.getTeamsList(this.page, $event)
    }
  }

  playerdetail(id) {
    this.navCtrl.push(TeamdetailPage, {
      id: id
    })
  }

}
