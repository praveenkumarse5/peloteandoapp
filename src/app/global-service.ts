import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class GlobalService {
    basepath: string = "https://football.innotical.com/api/";
    imgpath: string = "https://football.innotical.com/";
    header: Headers

    constructor(private http: Http) {

    }

    loginValid(url: string, data: any) {
        this.header = new Headers()
        let nameandpass = data.user + ":" + data.pass
        let encode = btoa(nameandpass)
        this.header.append("authorization", "Basic " + encode)
        return this.http.get(url, {
            headers: this.header
        })
    }

    postRequest(url, data) {
        this.header = new Headers();
        let user = JSON.parse(localStorage.getItem('user'));
        this.header.append("Authorization", "Bearer " + user.token);
        this.header.append("Content-Type", "application/json");
        return this.http.post(url, data, {
            headers: this.header
        })
    }

     getRequest(url) {
        this.header = new Headers();
        let user = JSON.parse(localStorage.getItem('user'));
        this.header.append("Authorization", "Bearer " + user.token);
        this.header.append("Content-Type", "application/json");
        return this.http.get(url,{
            headers: this.header
        })
    }
}